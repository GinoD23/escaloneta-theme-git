# Argento Theme for linux terminal

## How use 
1. First, install [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh) 

2. Download the file scaloneta.zsh-theme

3. Move the file to the folder /home/your-user/.oh-my-zsh/themes/

4. In the file /home/your-user/.zshrc, look for the line that says ZSH_THEME="", if it is 
commented out with "#", uncomment it so it looks like this: ZSH_THEME="scaloneta".

5. Save the file and in a terminal, use the command:
```bash
	source .zshrc
```
# Theme Picture

![Alt text](<Captura desde 2024-05-16 00-27-31.png>)

# easter egg
![d10s](<Captura desde 2024-05-16 00-28-24.png>)
![diegito](<Captura desde 2024-05-16 00-28-09.png>)
